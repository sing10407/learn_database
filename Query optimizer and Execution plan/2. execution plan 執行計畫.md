
基本語法:

```sql
EXPLAIN
# 下面接查詢語法
SELECT itemid,CHANNEL,mallname,title FROM goods
JOIN mallstore ON mallstore.mallid=goods.channel;
```

**Mysql 5.6 以上提供 optimizer trace 的功能，可以詳細看到 optimizer 的逐步執行計畫**

# EXPLAIN Output Columns


| Column                            | Meaning                                                         |
| --------------------------------- | --------------------------------------------------------------- |
| [id](#id)                         | The SELECT identifier 就是 ID，代表執行順序。                   |
| [select_type](#select-type) `***` | The SELECT type 如 union/derived/subquery 等訊息                |
| [table](#table)                   | The table for the output row 此筆計畫使用的 table               |
| [partitions](#partitions)         | The matching partitions                                         |
| [type](#type) `***`               | The join type  table 使用何種方式 join                          |
| [possible_keys](#possible-keys)   | 可能會用到的 indexes  不要看                                    |
| [key](#key) `***`                 | 實際上用到的 indexe                                             |
| [key_len](#keylen)                | 選擇的 key 的長度  (bytes)                                      |
| [ref](#ref)                       | The columns compared to the index 要被拿來查 index 的欄位或常數 |
| [rows](#rows) `***`               | 估計會被檢查(examined)的筆數                                    |
| filtered                          | 被條件過濾掉的筆數比例%                                         |
| [Extra](#extra)  `***`            | 其他資訊                                                        |

### id

就是 ID，代表執行順序。當值是 NULL，代表可能是用 union 的結果。

[back to top](#explain-output-columns)

### select type

| select_type Value    | Meaning                                                                                                   |
| -------------------- | --------------------------------------------------------------------------------------------------------- |
| SIMPLE               | Simple SELECT (not using UNION or subqueries)                                                             |
| PRIMARY              | Outermost SELECT                                                                                          |
| UNION                | Second or later SELECT statement in a UNION                                                               |
| DEPENDENT UNION      | Second or later SELECT statement in a UNION, dependent on outer query                                     |
| UNION RESULT         | Result of a UNION.                                                                                        |
| SUBQUERY             | First SELECT in subquery                                                                                  |
| DEPENDENT SUBQUERY   | First SELECT in subquery, dependent on outer query                                                        |
| DERIVED              | Derived table                                                                                             |
| MATERIALIZED         | Materialized subquery                                                                                     |
| UNCACHEABLE SUBQUERY | A subquery for which the result cannot be cached and must be re-evaluated for each row of the outer query |
| UNCACHEABLE UNION    | The second or later select in a UNION that belongs to an uncacheable subquery (see UNCACHEABLE SUBQUERY)  |

- `SIMPLE`：進行不需要 UNION 或不含子查詢的 SELECT 查詢。最外側的 SELECT 通常為 SIMPLE。
- `PRIMARY`：需要 UNION 或含子查詢的 SELECT 查詢計畫中，最外層的單位，查詢的 select_type 就會是 PRIMARY。
- `UNION`：union query 的 EXPLAIN outputs 中，除了第一個以外，第二個以後的所有 SELECT 都是 UNION。第一個則是 `DERIVED`。`DERIVED` 是一個臨時表，儲存 UNION 的查詢結果。
- `DEPENDENT`: [correlated subquery](https://dev.mysql.com/doc/refman/5.7/en/correlated-subqueries.html) 的使用
- `DEPENDENT SUBQUERY`：類似下面這種 query 中的子查詢
  ```sql
  select e.name, (select count(*) from xxx) as count
  from e
  ```
  - `DEPENDENT SUBQUERY` vs `UNCACHEABLE SUBQUERY`: DEPENDENT SUBQUERY 只會根據外層的各個變數(each set of different values of the variables)去 re-evaluated 一次；但對 UNCACHEABLE SUBQUERY 來說，外層的每一個 row 都會被 re-evaluated 一次。
- `DERIVED`：在 MySQL 5.5 或 MariaDB 5.2 中，`FROM` 中的子查詢都會被視為 DERIVED。但從 MySQL 5.6, MariaDB 5.3 開始，optimizer 選項 optimizer_switch，可能會將 `FROM` 中的子查詢與外部查詢一起優化。DERIVED 意味著要為查詢結果做臨時表(在記憶體或硬碟)。在 MySQL 5.5 與 MariaDB 5.2 中， DERIVED Table 沒有任何索引；從 MariaDB 5.3 開始，可以根據 optimizer 選項等向創建的臨時表增加索引。
- `UNCACHEABLE SUBQUERY`：即使查詢 sql 中只有一個子查詢，但該子查詢可能會被使用多次。執行相同子查詢時，前一次的子查詢會被存到記憶體。以下幾種狀況會無法使用 cache：
  - 子查詢含有使用者變數
  - 子查詢含有 NOT-DETERMINISTIC 屬性
  - 子查詢含有 UUID() 或 RAND() 等隨機值函數
- `MATERIALIZED`：這是從 MariaDB 5.3 與 MySQL 5.6 開始引入的一個 select_type，主要用於優化 FROM 子句或 IN(subquery) 的子查詢。主要會將查詢的結果具體化(Materialization)為臨時表。
- `Subquery cache` vs `query cache`
  - Subquery cache 是在執行當下使用
  - query cache 是查詢完成後的 cache
    - [8.10.3.1 How the Query Cache Operates](https://dev.mysql.com/doc/refman/5.7/en/query-cache-operation.html)

[back to top](#explain-output-columns)

### table

- 使用的 table 名稱
- 以下為臨時表：
  - `<unionM,N>` : 使用 union 指令。M, N 表示兩個 table 的 id
  - `<derivedN>` : 從某 table N 繼承來的結果 id。例如 subquery。N可以回去 Explain output id 去查該 row
  - `<subqueryN>`: 從 materialized subquery 結果來的 id。
- [8.2.2.2 Optimizing Subqueries with Materialization](https://dev.mysql.com/doc/refman/5.7/en/subquery-materialization.html)

[back to top](#explain-output-columns)

### partitions

顯示 query 會搜到的 partitions。非 partition 會顯示 NULL。

[back to top](#explain-output-columns)

### type

Join type。這個欄位會顯示 table 使用何種方式 joined。可能是使用 index 或是 table scan。進行查詢效能優化時，重要的一步就是查看 index 使用是否最佳化。所以執行計畫的 type 訊息很重要。

ref: [EXPLAIN Join Types](https://dev.mysql.com/doc/refman/5.7/en/explain-output.html#explain-join-types)

下列方法中，除了 `index_merge` 外，其他方法都只能用一種 index。 MySQL Optimizer 會分析計算 join type 與 cost，選擇出 cost 最小的一個 join type。

以下列出 join types，排序由最好到最爛：

1. `system` ： table 只有一筆資料或是沒有資料。這是 `const` join type 的個案。只適用於 MyISAM/Memory，不適用於 InnoDB/XtraDB。
2. `const` ： 查詢中含有使用 pk 或是 unique key 的 where 條件中，通常使用 const。讀這個 table 時最多只有一筆 match 資料。因為只有一筆資料，這個值被 optimizer 當為 constants。 const tables 非常快，因為只需要被讀一次。範例 query：
    ```sql
    SELECT * FROM tbl_name WHERE primary_key=1;
    ```
3. `eq_ref` ： 在此 table 需要讀到的 row 是根據前一個 table 的查詢結果。 除了 `system`, `const` 這兩個 join types 外，這是最佳的 join type。 `eq_ref` 被使用，代表所有關聯都有用到 index，而且 index 為 PK 或 Unique not null index。如果是複合鍵(Composite Key)，則需要該 index 所有欄位都在條件中才會使用。當 indexed column 被比較時用了 `=`，`eq_ref` 就會被使用。如下範例： 
    ```sql
    SELECT * FROM ref_table,other_table
    WHERE ref_table.key_column=other_table.column;

    SELECT * FROM ref_table,other_table
    WHERE ref_table.key_column_part1=other_table.column
    AND ref_table.key_column_part2=1;
    ```
4. `ref` ：`ref` 被使用時，代表 join 只使用 leftmost prefix of the key，或是 key 不是 PK 或 Unique index (換句話說，join 時無法根據 key 取得對應的單一 row)。使用 `ref` 時不受索引類型影響。 `ref` 無法保證必有 1 record 返回。但因為只對相等(`=` or `<=>`)進行比較，因此也是一種非常快的 join type。如果 key 只對應到很少的 rows，那 `ref` 也是不錯的 join type。 `ref` 可以使用在 indexed columns，並使用 `=` or `<=>` 等運算子。範例：
   ```sql
   SELECT * FROM dept WHERE dept_no='d005'
   ```
5. `fulltext`：全文檢索。不常用也不建議用，需要全文檢索通常會推薦用 elasticsearch 或 solr 之類的軟體。
6. `ref_or_null` ：這個類似 `ref`，但會額外搜尋含有 null values 的資料。這個 join type 最常用在 subqueries。範例：
   ```sql
   SELECT * FROM ref_table
   WHERE key_column=expr OR key_column IS NULL;
   ```
   補充：[IS NULL Optimization](https://dev.mysql.com/doc/refman/5.7/en/is-null-optimization.html)
7. `index_merge`：會從兩個以上的索引分別獲得搜尋結果，再 merge。需要注意這裡只會存取一個 table(但可以不同索引)。 Merge 可以是 unions, intersections, unions-of-intersections。
   
   範例：
   ```sql
   SELECT * FROM employee WHERE employee_id between 100001 and 11000 
   OR first_name ='Smith';
   ```
   ref: [Index Merge Optimization](https://dev.mysql.com/doc/refman/5.7/en/index-merge-optimization.html)
8. `unique_subquery`：這個會替換 `eq_ref`，為了 `IN` suqueries 的優化。子查詢只返回 unique values 時，就會使用此方法。這只是一個 index lookup function，用來取代 subqueries，達到更佳的效能：
   ```sql
   value IN (SELECT primary_key FROM single_table WHERE some_expr)
   ```
9.  `index_subquery`：對於 `IN` 運算子， `IN(subquerie)` 或 `IN(常數` 的條件，會先將括號內的重複值刪除。上一個 `unique_subquery` 因為可以保證不會產生重複值，所以不需要做此操作。：
   ```sql
   value IN (SELECT key_column FROM single_table WHERE some_expr)
   ```
11. `range`：傳說中的 `Range scan`。使用 index 搜尋，只有在指定區間內的資料會被回傳。explain output 中的 `key` 欄位會顯示對哪個 index 做 range 搜尋。 `key_len` 則代表搜尋過程中最長的 key。 `ref` 會是 null。
    `range` 在以下運算子對一個常數做比較時通常會用到： `=`, `<>`, `>`, `>=`, `<`, `<=`, `IS NULL`, `<=>`, `BETWEEN`, `LIKE`, or `IN()`
    ```sql
    SELECT * FROM tbl_name
    WHERE key_column = 10;
   
    SELECT * FROM tbl_name
    WHERE key_column BETWEEN 10 and 20;

    SELECT * FROM tbl_name
    WHERE key_column IN (10,20,30);

    SELECT * FROM tbl_name
    WHERE key_part1 = 10 AND key_part2 IN (10,20,30);
    ```
12.  `index`：注意不要把這個 index 與 `索引index` 搞混。他其實會對 index 做 full index scan。下列 case 滿足 1+2 或 1+3 就會使用 `index` 這個方法：
     - 無法使用 range, const, ref 作為 join type
     - 可以使用索引就達成搜尋(不需要 table scan)
     - 可以使用索引就達到 order by 或 group by(不需要額外排序)
  
      範例：
      因為不含 Where 條件，所以無法使用 range, const, ref。但因為 dept_name 有建索引，所以可以使用 `index` join type 來避免額外的排序。

      ```sql
      SELECT * FROM department
      ORDER BY dept_name DESC
      LIMIT 10;
      ```

13.  `ALL`：代表執行了 full table scan，會從頭到尾讀取 table 的資料，再刪除不需要的資料。InnoDB 提供一次讀取多個 Disk pages 的功能，所以當進行 full table/index scan 這類需要大量 DISK I/O 操作時相當有用。 InnoDB 稱為 Read Ahead。像是資料倉儲或批次處理程式，需要處理大量資料的查詢中，ALL 的 join type 可能會比其他調教失敗的查詢(查詢時強制使用索引)更好。查詢優化意思並不是無條件禁用 full table/index scan。

[back to top](#explain-output-columns)


### possible keys

執行計畫中，Optimizer 會考慮多種處理方法，然後選擇一個 cost 的執行計畫。 Optimizer 會從選定的訪問方式中，列出索引的候選列表，這就是 possible keys 內容，並非列出的所有索引都會用到。從執行計劃來看，table 的所有索引都會出現，對於查詢沒什麼用處，因此查看執行計畫時可以忽略這個欄位。

[back to top](#explain-output-columns)

### key

possible keys 是候選索引， key 則是最終執行計畫所用的索引。 因此進行執行計畫的優化時，一定要看 key 欄位中是不是想要的索引。值為 PRIMARY 時代表使用 pk，其他值則為建立索引所取的名稱。

如果執行計畫的 join type 是 index_merge 時，可以使用兩個以上所引；否則每個 table 只能用一個索引。

[back to top](#explain-output-columns)

### key_len

這個欄位表示 key 的長度(bytes)。如果 `key` 欄位是 null，則此欄位也會是 null。

下面這個例子中，使用到的 key : dept_no CHAR(4), employee_id Integer，因此 key_len 是 CHAR(4) + Integer = 12 + 4 = 16。(dept_no 用 utf8，一個 char 佔 1~3 bytes；integer 佔 4 bytes)

```sql
SELECT * FROM dept_employee 
WHERE dept_no='d005' and employee_id=1001;
```

[back to top](#explain-output-columns)

### ref

此欄位顯示提供哪種值作為參考條件(equal 比較條件時)。若指定常數則會顯示 const，若為 table 則會顯示 table name。有一種 case 會顯示 func，代表 function，表示先經過運算後再使用，例如：
```sql
WHERE e.employee_id = (de.employee_id - 1)
```

[back to top](#explain-output-columns)


### rows

對於各種條件，MySQL Optimizer 會列出可能的處理方式，並計算各個的 cost，選擇一個最小 cost 的執行計畫。在計算比較時，會對各種方式需要的 record 數進行預測，包含查看 table 中有多少 records 等，以統計訊息為基礎進行預測。要注意這些數值通常是預先計算的統計數值，並非精準的數字。

假設現在有一個 query:
```
EXPLAIN
SELECT * FROM dept_employee WHERE from_date >= '1985-01-01'
```

回傳的執行計畫中， `rows`欄位值假設為 331603，這表示 MySQL 處理時大約要讀取 331603 筆資料。而這個 table 實際上資料量也是 331603，因此可以判斷 optimizer 採用了 full table scan。

接下來若縮小查詢範圍
```
EXPLAIN
SELECT * FROM dept_employee WHERE from_date >= '2019-07-01'
```
此時`rows`欄位值假設為 292，代表 optimizer 推測 from_date 值大於 '2019-07-01' 的資料只有 292 筆，因此 optimizer 就會使用 range 搜尋。

[back to top](#explain-output-columns)

### Extra

Extra 中通常包含與效能相關的重要訊息，會顯示 MySQL 如何解決這個 query。

訊息：
- `const row not found`
- `Distinct`
- `Full scan on NULL key`
- `Impossible HAVING`
- `Impossible WHERE`
- `Impossible WHERE noticed after reading const tables`
- `NO matching min/max row`
- `no matching row in const table`
- `NO tables used`
- `Not exists`
- `Range checked for each record(index map:N)`
- `Scanned N databases`
- `Select tables optimizer away`
- `Skip_open_table, Open_frm_only, Open_trigger_only, Open_full_table`
- `unique row not found`

- `Using filesort`：一般處理 order by 時可以使用索引，但沒有適合索引可以用時，就需要額外的一次排序。只有無法使用索引處理 order by 時，Extra 才會出現 using filesort。系統會將 records 複製到排序用的 sort buffer，並使用 quick sort 排序。 using filesort 只會出現在帶有 order by 的查詢。`Using filesort` 是 MySQL 裡一種比較慢的外部排序，對效能有重要影響，可以優先優化。
  - 延伸：[ORDER BY Optimization](https://dev.mysql.com/doc/refman/5.7/en/order-by-optimization.html)
- `Using index`
- `Using index for group-by`
- `Using join buffer`
- `Using sort_union, using union, using intersect, using sort_intersect`
- `Using temporary`：需要去開 temporary table 存資料，通常發生在 query 含有 `GROUP BY` 或 `ORDER BY`。
  ```sql
  SELECT * FROM employee GROUP BY gender ORDER BY MIN(employee_id)
  ```
  如上述查詢中， Group by 無法使用索引，執行計畫的 Extra 就會顯示 using temporary。
  temporary table 可能存在 memory 或 disk。一開始會將 temporary table 建立在 Memory，隨著資料量變大會移動到 disk。若建立 Disk，MariaDB 會使用 Aria engine； MySQL 與 Percona 會使用 MyISAM engine。

  可以使用下列語法查詢 temporary table 是在 memory 或 disk 建立的：
  ```sql
  SHOW SESSION STATUS LIKE 'Created_tmp%
  ```
  | Variable_name           | Value |
  | ----------------------- | ----- |
  | Created_tmp_disk_tables | 0     |
  | Created_tmp_files       | 24037 |
  | Created_tmp_tables      | 1     |

  查詢結果中的 Created_tmp_disk_tables 就是在 disk 建立的 temporary table 數量。有兩個情境會建立在 disk：
  - 存在 temporary table 的資料大小超過 MySQL 變數 `max_heap_table_size`, `tmp_table_size`(取兩個變數最小值)
  - 資料包含 `BLOB` 或 `TEXT` 的型態

  注意：很多時候雖然 Extra 沒有顯示 using temporary，但其實內部也使用了 temporary table。建立 temporary 的代表性情境如下：
  - From 子句的子查詢必定建立 temporary table(Derived table)。
  - 含有 count(distinct column) 的查詢無法使用索引時
  - 使用 union / union all
  - 不能使用索引的 order by 條件，見Using filesort
- `using where`
- `using where with pushed condition`
- `deleting all rows`
- `FirstMatch(tbl_name)`
- `LooseScan(m...n)`
- `Materialize, scan`
- `Start materialize, end materialize, scan`
- `Start temporary, end temporary`
- `using index condition`
- `rowid-ordered scan, key-ordered scan`
- `no matching rows after partition pruning`


[back to top](#explain-output-columns)


# references
- https://dev.mysql.com/doc/refman/5.7/en/optimization.html
- https://dev.mysql.com/doc/refman/5.7/en/explain-output.html
- 深入理解MariaDB與MySQL
- https://www.fromdual.com/avoid-temporay-disk-tables-with-mysql
- [The MySQL Query Optimizer](file/The%20MySQL%20Query%20Optimizer.pdf)
