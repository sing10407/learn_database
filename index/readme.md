# 前言

DBMS 所管理的資料通常很大，需要存在 Disk。 Disk 存取單位是 `page`。

| record | id    | name                  | price | -   |
| ------ | ----- | --------------------- | ----- | --- |
| page 1 |
| 1      | b3001 | 資料庫理論            | 400   | -   |
| 4      | c4022 | Spark 導論            | 450   | -   |
| 3      | c2311 | javascript 入門到放棄 | 399   | -   |
| 2      | g1234 | 轉行賣雞排            | 888   | -   |
| page 2 |
| 7      | k3501 | 航海王                | 410   | -   |
| 6      | j4122 | 鬼滅之刃              | 150   | -   |
| 5      | z2511 | 非洲的動物上班族      | 393   | -   |
| 8      | e1434 | 進擊的巨人            | 338   | -   |

- 一個 table 是由數個 pages 組成
- 一個 pages 包含數筆 records
- 在硬碟中，同一個 table 的 pages 不一定連續
- page 與 page 間使用 linked list 鏈結
- page 中的 records 也不一定連續

![](img/pages.jpg)

DBMS 中會記錄每一個 Table 第一個 page 的位子，與各個 attributes 的順序和型態，稱為資料字典 (Data dictionary)

![](img/dictionary.jpg)



# B+ tree

![](img/bplustree.jpg)

- internal node 不存資料，因此可以在一個 disk page 塞多一點 nodes，減少 disk io 與 cache misses。
- leaf node 會存資料，每個 leaf 中間有 link。因此對於 range search 速度很快。
  - leaf 資料存的是 <key, data pointer>，加 sibling pointer
    - data pointer 會指到 disk page 和 record
  - 因為每個 leaf nodes 相互連結，因此要做 full scan 只需要 O(n)

[B+ tree visualization](https://www.cs.usfca.edu/~galles/visualization/BPlusTree.html)


![](img/index_overview.jpg)

# References

- https://stackoverflow.com/questions/870218/differences-between-b-trees-and-b-trees#
- https://cloud.tencent.com/developer/article/1403064
- https://www.mis.nsysu.edu.tw/db-book/PDF/Ch9.pdf
- https://mp.weixin.qq.com/s?__biz=MjM5ODYxMDA5OQ%3D%3D&mid=2651961486&idx=1&sn=b319a87f87797d5d662ab4715666657f
- 資料庫的核心理論與實務(黃三益)